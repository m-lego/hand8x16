ortho 8x16
==========

this started from my desire to convert an tipro pos into a qmk keyboard.



![8x16](figs/8x16front.jpg)



pcbs


![pcb front](figs/hand8x16-pcb.png)

![pcb back](figs/handwire8x16-bottom.png)


![8x16](figs/8x16back.jpg)
